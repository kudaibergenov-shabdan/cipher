import {EDIT_MESSAGE_ATTRIBUTE} from "../actions/action";

const initialState = {
    encodedMessage: "",
    decodedMessage: "",
    password: ""
};

const messageReducer = (state = initialState, action) => {
    switch (action.type) {
        case EDIT_MESSAGE_ATTRIBUTE:
            const key = Object.keys(action.payload)[0];
            return {...state, [key]: action.payload[key]};
    }
    return state;
};

export default messageReducer;