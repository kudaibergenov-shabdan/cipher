import React from 'react';
import './MessageCipher.css';
import {useDispatch, useSelector} from "react-redux";
import {editMessage, runDecodeMessage, runEncodeMessage} from "../../actions/action";

const MessageCipher = () => {
    const dispatch = useDispatch();
    const encodedMessage = useSelector(state => state.messageReducer.encodedMessage);
    const decodedMessage = useSelector(state => state.messageReducer.decodedMessage);
    const password = useSelector(state => state.messageReducer.password);

    const onInputChange = e => {
        const {name, value} = e.target;
        dispatch(editMessage(name, value));
    };

    const encodeMessage = () => {
        if (password) {
            dispatch(runEncodeMessage(decodedMessage, password));
        }
    };

    const decodeMessage = () => {
        if (password) {
            dispatch(runDecodeMessage(encodedMessage, password));
        }
    }

    return (
        <div className="MessageCipher">
            <div className="input-block">
                <input
                    className="input-message"
                    type="text"
                    name="decodedMessage"
                    placeholder="decoded message"
                    value={decodedMessage}
                    onChange={onInputChange}
                />
            </div>
            <div className="input-block">
                <input
                    type="text"
                    name="password"
                    placeholder="Password"
                    value={password}
                    onChange={onInputChange}
                />
                <button onClick={encodeMessage}>Encode</button>
                <button onClick={decodeMessage}>Decode</button>
            </div>
            <div className="input-block">
                <input
                    className="input-message"
                    type="text"
                    name="encodedMessage"
                    placeholder="encoded message"
                    value={encodedMessage}
                    onChange={onInputChange}
                />
            </div>
        </div>
    );
};

export default MessageCipher;