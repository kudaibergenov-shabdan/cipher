import axios from "axios";

export const EDIT_MESSAGE_ATTRIBUTE = 'EDIT_MESSAGE_ATTRIBUTE';

const editMessageAttribute = (message) => ({type: EDIT_MESSAGE_ATTRIBUTE, payload: message});

export const editMessage = (attr, value) => {
    return editMessageAttribute({
        [attr]: value
    });
};

export const runEncodeMessage = (decodedMessage, password) => {
    return async dispatch => {
        try {
            const response = await axios.post('http://localhost:8000/encode',  {
                "message": decodedMessage,
                "password": password
            });
            dispatch(editMessage('encodedMessage', response.data['encoded']));
        } catch (e) {
            console.log('Something went wrong in method runEncodeMessage: ', e);
        }
    }
};

export const runDecodeMessage = (encodedMessage, password) => {
    return async dispatch => {
        try {
            const response = await axios.post('http://localhost:8000/decode',  {
                "message": encodedMessage,
                "password": password
            });
            dispatch(editMessage('decodedMessage', response.data['decoded']));
        } catch (e) {
            console.log('Something went wrong in method runEncodeMessage: ', e);
        }
    }
}
