import MessageCipher from "./components/MessageCipher/MessageCipher";

const App = () => (
    <MessageCipher />
);

export default App;
