const express = require('express');
const cors = require('cors');
const port = 8000;
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
app.use(express.json());
app.use(cors());

app.post('/encode', (req, res) => {
  if (!req.body.password || !req.body.message) {
    return res.status(400).send({error: "Not valid data"});
  }
  const cipher = Vigenere.Cipher(req.body.password).crypt(req.body.message);
  res.send({'encoded': cipher});
});

app.post('/decode', (req, res) => {
  if (!req.body.password || !req.body.message) {
    return res.status(400).send({error: "Not valid data"});
  }
  const decipher = Vigenere.Decipher(req.body.password).crypt(req.body.message);
  res.send({'decoded': decipher});
});

app.listen(port, () => {
  console.log(`Server started on port: ${port} successfully`);
});

